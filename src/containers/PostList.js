import React from 'react';
import Post from '../components/Post';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

class PostList extends React.Component {
    constructor(props){
        super(props);
       
    }
    
    render(){
        return(
           <div>
               {this.props.posts.map(post => (
                    <Post post= {post}/>
               ))}
               </div>
        )
    }
    
}

PostList.propTypes = {
    posts: PropTypes.object.isRequired
}

function mapStateToProps(state) {
    return { posts:state.posts };
}

export default connect(mapStateToProps)(PostList);