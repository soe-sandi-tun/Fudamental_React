import React from 'react';
import AddPostFormComponent from '../components/AddPostForm'
import {connect} from 'react-redux';
import {addPost} from '../actions/index';

class AddPost extends React.Component{
    constructor(props){
        super(props)
    }

    render(){
        return (
            <AddPostFormComponent onSubmit = {this.props.onAddPost}/>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        onAddPost : post => {
            dispatch(addPost(post))
            
        }
    }
  }

export default connect(null,mapDispatchToProps)(AddPost);