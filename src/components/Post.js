import React from 'react';
import PropTypes from 'prop-types';
const Post = (props) => {
    return(
        <React.Fragment>
            <h1>Title {props.posts.title}</h1>
        </React.Fragment>
    )
}

Post.propTypes = {
    posts: PropTypes.object.isRequired
}


export default Post;