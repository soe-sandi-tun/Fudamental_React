import React from 'react';
import {connect} from 'react-redux';
import {addPost} from '../actions/index';
import PropTypes from 'prop-types';

class AddPostForm extends React.Component{
    constructor(props){
        super(props);
        this.state ={
            title: '',
            content: ''
        }
        this.onAddPost = this.onAddPost.bind(this);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.onChangeContent = this.onChangeContent.bind(this);
    }
    onChangeTitle(e){
         this.setState({title : e.target.value})
    }
    onChangeContent(e){
        this.setState({content: e.target.value})
    }
    onAddPost(event){
       
        const post = {
            title: this.state.title,
            content: this.state.content
        }
        this.props.onSubmit(post);
        
    }
    render(){
        return(
            
            <form onSubmit= {this.onAddPost()}>
                <input type = "text" required placeholder="Enter Title" onChange = {this.onChangeTitle}/>
                <textarea rows = "7" cols = "100" required placeholder="Enter post content" onChange= {this.onChangeContent}/>
                <button type = "submit" >Post</button>
                </form>
            
        )
    }
}

AddPostForm.propTypes = {
    onSubmit: PropTypes.func.isRequired
}
export default AddPostForm;