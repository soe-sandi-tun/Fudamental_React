import  {ADD_POSTS} from '../actions/actionType';

const initialPost = [
    {
        id: 1,
        title : 'post 1',
        content : 'Please be ok'
    }
]
export default function (state = initialPost, action){
    switch(action.type){
        case ADD_POSTS: 
        // return [...state, {id: action.payload.id, title: action.payload.title, content: action.payload.content}];
        return [...state, action.payload];

        default :
        return state;
    }

}