import  {ADD_USERS,FETCH_USERS,UPDATE_USERS,DELETE_USERS} from '../actions/actionType';
export default function (state = [], action){
    switch(action.type){
        case ADD_USERS: 
        return [...state, {name: action.name}];

        default :
        return state;
    }

}