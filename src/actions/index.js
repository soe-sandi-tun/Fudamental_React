import {ADD_POSTS,ADD_USERS,FETCH_POSTS,FETCH_USERS,UPDATE_POSTS,UPDATE_USERS,DELETE_POSTS,DELETE_USERS } from './actionType';
import { ADD_BOOKMARK, DELETE_BOOKMARK } from './types';
let id = 0;
export const addPost = ({title,content}) => ({
    type:ADD_POSTS,
    payload: {
        id: id++,
        title: title,
        content: content
    }
});

export const fetchPost = (post) => ({
    type:FETCH_POSTS,
    payload: {
        post
    }
})


export const addBookmark = ({ title, url }) => ({
  type: ADD_BOOKMARK,
  payload: {
    id: id++,
    title,
    url
  }
});

export const deleteBookmark = id => ({
  type: DELETE_BOOKMARK,
  payload: {
    id
  }
});