import React, { Component } from 'react';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom';
import AddBookmark from './containers/AddBookmark';
import BookmarksList from './containers/BookmarksList';


class App extends Component {
  render() {
    return (
      <div className="App">
        {/* <Router >
          <Switch>

          <Route exact path = "/posts" component = {PostListContainer} />
          <Route path = "/addPost" component = {AddUserContainer} />
          </Switch>
          </Router> */}
          <AddBookmark />
        <BookmarksList />
      </div>
    );
  }
}

export default App;
